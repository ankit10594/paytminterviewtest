import { gql } from "@apollo/client";

export const CONTINENT_LIST = gql`query {
    continents {
      code
      name
    }
  }  
`;

export const COUNTRY_BASED_ON_CONTINENT = gql`query GetCountries($code: String) { 
    countries(filter:  {
        continent:{
          eq: $code
        }
      }) {
        code, name, languages {
            code, name, rtl
          }
      }
    }
  
`;
