import { ContinentType } from '../models/ContinentType';
import { useQuery } from "@apollo/client";
import { CONTINENT_LIST } from '../queries';
import '../styles/styles.css'
export interface ContinentsPropsType {
    onClick:  (code: string) => void,
    selectedContinent: string
}

export default function Continents({onClick, selectedContinent}: ContinentsPropsType) {
  const {data: continentData, loading : continentLoading, error: contitnentError } = useQuery<ContinentType>(CONTINENT_LIST);
  if(continentLoading) return <div><p>Loading Continent</p></div>
  if(contitnentError) return <div><p>Error:  {contitnentError.message}</p></div>
  return <><div className='list-group-item list-group-item-action bg-secondary text-white '>
            Select Continent
        </div>
        <div className="list-group maxHeighDiv bg-light border"> 
        {continentData && continentData.continents?.map((item: any) => <button onClick={()=>{
            onClick(item.code)
        }} className={`list-group-item list-group-item-action ${selectedContinent === item.code ? "active" : null}`} aria-current="true">{item.name}</button>)}
  </div></>
}
