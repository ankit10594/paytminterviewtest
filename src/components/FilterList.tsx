import React from 'react'
import { LanguagesInterface } from '../models/CountriesType';
export interface FilterListPropsType {
    onChangeValue:  (item: any) => void,
    list: LanguagesInterface[]
}
export default function FilterList({list, onChangeValue}:FilterListPropsType) {
  return (
    <div className="bg-light card p-2 ">
        <p>Filter Countries by Language</p>
        <select onChange={(e)=>{
            onChangeValue(e.target.value)
        }} className='form-control' placeholder='Select Langauge to Filter'>
            <option value="0">Chosse One</option>
            {list.map((item)=>{
                return <option value={item.code}>{item.name}</option>
            })}
        </select>
    </div>
  )
}
