import React,{useEffect, useState} from 'react'
import { CountryType } from '../models/CountriesType';

import { useQuery } from "@apollo/client";
import { COUNTRY_BASED_ON_CONTINENT } from '../queries';
import '../styles/styles.css'
import { Country_all } from '../models/CountriesType';
import FilterList from './FilterList';

import _ from 'lodash'


export interface CountriesPropsType {
    onClick:  (code: string) => void,
    selectedContinent: string,
    selectedCountry:Country_all
}

export default function Countries({onClick, selectedContinent, selectedCountry}: CountriesPropsType) {
  const {data, loading , error } = useQuery<CountryType>(COUNTRY_BASED_ON_CONTINENT, {variables : {code: selectedContinent}});
  const [countries, setCountries] = useState<any>([])
  const [filterList, setFilterList] = useState<any>([])

  useEffect(()=>{
    setCountries(data?.countries)
    const filterListLanguage =  data?.countries?.map((item: any) => { return item?.languages });
    const arrayOfObjs = filterListLanguage?.reduce((acc:any, obj : any) => {
        obj.forEach((item: any)=> acc.push(item));
        return acc
    }, [])
    setFilterList(_.uniqBy(arrayOfObjs, 'code').sort())
  },[ data?.countries])

  const FilterValues = (item: any) => {
    if(item !== "0"){
        const newFIlteredList = data?.countries?.reduce((acc: any, current: any)=>{
          if(current.languages.some((item1: any) => { 
            return item1.code === item})){
                  acc.push(current)
            }
            return acc
      },[])
      setCountries(newFIlteredList)
    }else{
      setCountries(data?.countries)
    }
   
     
  }

  if(loading) return <div><p>Loading Countries</p></div>
  if(error) return <div><p>Error:  {error.message}</p></div> 

  
  return <><div className={`list-group-item list-group-item-action bg-secondary text-white border-0`}>Select Country</div><div className={`list-group maxHeighDiv bg-light border`}> 
        <FilterList list={filterList} onChangeValue={(item)=>{
            FilterValues(item)
        }} />
        {countries && countries?.map((item: any) => <button onClick={()=>{
            onClick(item)
        }} className={`list-group-item list-group-item-action ${item.code === selectedCountry.code ? "active" : ""}`} aria-current="true">{item.name}</button>)}
  </div></>
}
