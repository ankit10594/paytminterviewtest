import { Country_all } from '../models/CountriesType';

import '../styles/styles.css'

export interface LanguagesPropsType {
    onClick:  (code: string) => void,
    list: Country_all
}

export default function Languages({onClick, list}: LanguagesPropsType) {
  console.log("LANGUAGE_BASED_ON_COUNRTY",list)


  return <><div className={`list-group-item list-group-item-action bg-secondary text-white border-0`}>Language used in {list.name}</div><div className={`list-group maxHeighDiv bg-light border`}> 
        {list && list?.languages?.map((item: any) => <button onClick={()=>{
            onClick(item)
        }} className={`list-group-item list-group-item-action`} aria-current="true">{item.name}</button>)}
  </div></>
}
