import React from 'react';
import './App.css';
import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider
} from "@apollo/client";

import Home from './pages/Home/index'


function App() {
  const client = new ApolloClient({
    uri: 'https://countries.trevorblades.com/',
    cache: new InMemoryCache()
  });

  return (
   <ApolloProvider client={client}>
    <Home />
  </ApolloProvider>
  );
}

export default App;
