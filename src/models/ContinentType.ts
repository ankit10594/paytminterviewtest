export interface Contitnent_all {
  __typename: "Contitnent";
  code: string | null;
  name: string | null;
}

export interface ContinentType {
  continents: (Contitnent_all | null)[] | null;
}