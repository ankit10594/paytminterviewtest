export interface LanguagesInterface  {
    code : string,
    name: string,
    rtl: boolean
}

export interface Country_all {
    __typename: "Country";
    code: string | null;
    name: string | null;
    languages : LanguagesInterface[]
  }

  export interface CountryType {
    countries: (Country_all | null)[] | null;
  }