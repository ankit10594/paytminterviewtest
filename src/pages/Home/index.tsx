import React,{useState} from 'react'

import  Continents  from '../../components/Continents';
import  Countries  from '../../components/Countries';
import  Langauges  from '../../components/Langauges';
const Home = () => {
  
 const [selectedContinent, setSelectedContinent ] = useState<string>("")
 const [selectedCountry, setSelectedCountry ] = useState<any>()

  
  return (
    <div className='container mt-5'>
        <div className='row'>
           <div className='col-4'>
                <Continents selectedContinent={selectedContinent} onClick={(code)=>{
                    setSelectedCountry("")
                    setSelectedContinent(code);
                }}/>
            </div>
            <div className='col-4'>
                {selectedContinent && <Countries selectedCountry={selectedCountry} selectedContinent={selectedContinent} onClick={(item)=>{
                    setSelectedCountry(item);
                }} />}
            </div>
            <div className='col-4'>
                {selectedCountry && <Langauges list={selectedCountry} onClick={()=>{
                   
                }} />}
            </div>
        </div>
        
    </div>
  )
}

export default Home;
