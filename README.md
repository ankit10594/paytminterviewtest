
# How to run the code?

* Clone the repository using `git clone https://gitlab.com/ankit10594/paytminterviewtest.git`
* run `npm install` in the terminal
* run `npm run start` and you are good to go

To access the Page open
`http://localhost:3000` in your browser


# Challenge

## Please write a simple application to do the following:

* Access the GraphQL endpoint - ​​https://countries.trevorblades.com/ to access Continents, Countries and Languages. You can access the endpoint through a browser to see the various tables and queries available.
Start a new react application using create-react-app (with typescript).

* Add a page to allow the user to select a continent.
Once a continent is selected, allow the user to select a country.

* Once the country has been selected, show the languages used in that country.

* Add a filter to restrict the list of countries by language: i.e. only show countries that use the filtered language.

Use whatever libraries you are comfortable using.

## Points to remember:
* Use TypeScript throughout.

* Keep the code modular.

* Avoid use of “let” and “for” statements.

* Separate business logic from tsx files.

## Bonus Points:
* Appealing visual presentation.

* Unit tests, although they don’t need to be extensive.
